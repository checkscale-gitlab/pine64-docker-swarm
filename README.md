# pine64-docker-swarm

`pine64-docker-swarm` will help you to setup a Docker Swarm cluster based on [PINE A64-LTS] boards.
The stuff is based on [Ansible] playbooks.

**Part1: build dedicated images**

The purpose of the first step is to build an image (.img) for each node of the cluster.
The based image should be produced by the side project [tmorin/pine64-image-armbian].

At the end of the step1, for each node, there will be a dedicated image containing:
- a configured hostname
- the network configuration (static IP, net mask, gateway, DNS)
- a user `pine64` only available from SSH with an authorized SSH key

**Part2: setup a ready to use cluster**

The purpose of the second step is to setup a ready to use cluster.

According to [inventory.ini], the cluster will be composed of:
- one main master
- any secondary managers
- any workers

The cluster will offer:
- [Keepalived]: load balance the ingress traffic over cluster's nodes
- [Ceph]: a distributed filesystem
- [influxdata]: a monitoring stack
- [Traefik]: reverse proxy with [Let's encrypt] support
- [Portainer]: management of docker stuff

**References**

The [Ansible] playbooks are heavily inspired by the following projects:

- https://github.com/sepich/ceph-swarm
- https://geek-cookbook.funkypenguin.co.nz
- https://github.com/rak8s/rak8s

## Getting started

### 1. Define the inventory

Each node of the cluster has to be configured with:

- a name (`<host name>`)
- a static IP (`<host ip>`)
- eventually the path to a OSD device for a [Ceph] cluster (`<sdcard path>`)

The node are defined in [inventory.ini] with the respect of the following pattern:

```
<host name> ansible_host=<host ip> [ceph_osd_device=<block path>]
```

Then, the node which will play the role of main manager must be registered in the group `[main_manager]`.

Finally, the nodes which will play the role of secondary managers must be registered in the group `[secondary_managers]`.

### 2. Override playbooks' variables

Playbooks are heavily based on variables which can be overridden when executed.
So that, the setup can fit as much as possible your requirements.
Eventually the playbook it-self could be changed if variable are not enough.

The variables are defined in the following fields:

- [group_vars/all.yml](group_vars/all.yml)
- [roles/ceph/vars/main.yml](roles/ceph/vars/main.yml)
- [roles/common/vars/main.yml](roles/common/vars/main.yml)
- [roles/image/vars/main.yml](roles/image/vars/main.yml)
- [roles/keepalived/vars/main.yml](roles/keepalived/vars/main.yml)
- [roles/monitoring/vars/main.yml](roles/monitoring/vars/main.yml)
- [roles/mount_cifs/vars/main.yml](roles/mount_cifs/vars/main.yml)
- [roles/portainer/vars/main.yml](roles/portainer/vars/main.yml)
- [roles/traefik/vars/main.yml](roles/traefik/vars/main.yml)

To override variables, create the file `custom_variables.yml`, and then edit it overriding the wished variables.

For instance:
```yaml
# override the network config
eth0_netmask: "255.255.255.0"
eth0_gateway: "192.168.1.1"
cluster_virtual_ip: "192.168.1.99"

# override the path to the SSH public key to push in the node's image
ssh_pub_key_path: "/home/someone/.ssh/id_rsa.pub"

# override the Traefik config to configure Let's Encrypt with my email
traefik_main_email: "me@my-domain.com"
```

### 3. Generate the system images

Once the inventory is completed and variable overridden accordingly, the images of each node can be generated.
The generation is handled by the playbook [images.yml].

```bash
./play.sh images.yml
```

`./play.sh` is a wrapper adding the option `--extra-vars "@custom_variables.yml"` automatically if `custom_variables.yml` exists.

At the end of the execution you will have in the current folder, one file `.img` by node.

Now, the images can be written to the sdcard/EMMC and then plugged to boards.
Finally, boards can be started.
Presently, with Pine64 boards the startup could be stuck due to ... I do not really know ...
Anyway, some time restart is enough, some time the image must be written again to the sdcard/EMMC.

### 4. Start cluster installation

Once all boards are ready, the cluster installation can be started.
You can rely on the [Ansible ping module] to check the board's status:

The installation is handled by the playbook [cluster-full.yml].

```bash
./play.sh cluster-swarm-only.yml
```

```bash
./play.sh cluster-ceph.yml
```

```bash
./play.sh cluster-traefik.yml
```

```bash
./play.sh cluster-portainer.yml
```

```bash
./play.sh cluster-monitoring.yml
```

[inventory.ini]: inventory.ini
[images.yml]: ./images.yml
[cluster-full.yml]: cluster-full.yml
[group_vars/all.yml]: ./group_vars/all.yml
[PINE A64-LTS]: https://www.pine64.org/?page_id=46823
[Ansible]: https://www.ansible.com
[Ansible ping module]: https://docs.ansible.com/ansible/latest/modules/ping_module.html
[tmorin/pine64-image-armbian]: https://gitlab.com/tmorin/pine64-image-armbian
[Traefik]: https://traefik.io
[Portainer]: https://portainer.io
[Let's encrypt]: https://letsencrypt.org
[Keepalived]: http://www.keepalived.org
[Ceph]: https://ceph.com
[influxdata]: https://www.influxdata.com
