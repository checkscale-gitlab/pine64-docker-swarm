version: '3.7'

networks:
  default:
    driver: overlay
    attachable: true
  traefik_public:
    external: true

configs:
  telegraf_config:
    file: ./telegraf.conf

services:
  influxdb:
    image: {{ monitoring_docker_image_influxdb }}
    environment:
      - INFLUXDB_DB=telegraf
      - INFLUXDB_LOGGING_LEVEL=warn
    volumes:
      - {{ monitoring_volume_influxdb_data }}:/var/lib/influxdb
    networks:
      - default
    deploy:
      replicas: 1
      resources:
        limits:
          memory: 400M
        reservations:
          memory: 400M

  telegraf:
    image: {{ monitoring_docker_image_telegraf }}
    environment:
      - HOST_PROC=/host/proc
    volumes:
      - /proc:/host/proc:ro
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - default
    hostname: "{{ "{{.Node.Hostname}}" }}"
    configs:
      - source: telegraf_config
        target: /etc/telegraf/telegraf.conf
    deploy:
      mode: global
      resources:
        limits:
          memory: 64M

  kapacitor:
    image: {{ monitoring_docker_image_kapacitor }}
    command: -log-level=error
    environment:
      - KAPACITOR_INFLUXDB_0_URLS_0=http://influxdb:8086
    volumes:
      - {{ monitoring_volume_kapacitor_data }}:/var/lib/kapacitor
    networks:
      - default
    deploy:
      replicas: 1
      resources:
        limits:
          memory: 64M
        reservations:
          memory: 64M

  chronograf:
    image: {{ monitoring_docker_image_chronograf }}
    ports:
      - target: 8888
        published: 9003
        protocol: tcp
    volumes:
      - {{ monitoring_volume_chronograf_data }}:/var/lib/chronograf
    networks:
      - default
      - traefik_public
    command: --log-level=error --influxdb-url=http://influxdb:8086 --kapacitor-url=http://kapacitor:9092
    deploy:
      labels:
        - "traefik.enable=true"
        # routers
        - "traefik.http.routers.monitoring.rule=Host(`{{ monitoring_host }}`)"
        - "traefik.http.routers.monitoring.entrypoints=https"
        - "traefik.http.routers.monitoring.tls.certresolver=letsencrypt"
        - "traefik.http.routers.monitoring.middlewares=basic_auth_admin@docker"
        # services
        - "traefik.http.services.monitoring.loadbalancer.server.port=8888"
      replicas: 1
      resources:
        limits:
          memory: 64M
        reservations:
          memory: 64M
